module "security_groups" {
  source = "../security"
}

module "vpc" {
  source = "../vpc"
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "wordpress_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*wordpress*"]
  }

  owners = ["amazon"]
}

data "aws_ami" "bastion_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]  
  }

  owners = ["amazon"]
}

resource "aws_instance" "wordpress_instance_a" {
  ami           = data.aws_ami.wordpress_ami.id
  instance_type = "t2.micro"
  subnet_id = module.vpc.wordpress_subnet_a_id
  vpc_security_group_ids =  module.security_groups.sg_wordpress_id
  availability_zone = element(data.aws_availability_zones.available.names, 0)
  

  tags = {
    Name = " Wordpress A"
  }
}

resource "aws_instance" "wordpress_instance_b" {
  ami           = data.aws_ami.wordpress_ami.id
  instance_type = "t2.micro"
  subnet_id = module.vpc.wordpress_subnet_b_id
  vpc_security_group_ids =  module.security_groups.sg_wordpress_id
  availability_zone = element(data.aws_availability_zones.available.names, 1)
  

  tags = {
    Name = " Wordpress B"
  }
}


## Création de la paire de clé du serveur  Bastion
resource "aws_key_pair" "myec2key" {
  key_name   = "datascientest_keypair"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.vastion_ami.id
  instance_type          = "t2.micro"
  subnet_id              = module.vpc.evaluation_public_subnet_a_id
  vpc_security_group_ids = module.security_groups.sg_bastion_id
  key_name               = "${aws_key_pair.myec2key.key_name}"

  tags = {
    Name        = "bastion_evaluation"
  }

}
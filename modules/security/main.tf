module "vpc" {
   source = "../vpc"
   
}

resource "aws_security_group" "sg_wordpress" {

   name   = "sg_wordpress"
   vpc_id = module.vpc.vpc_id

   ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    # Veuillez limiter votre entrée aux seules adresses IP et ports nécessaires.
    # L'ouverture à 0.0.0.0/0 peut entraîner des failles de sécurité.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "wordpress-sg"
  }
  
}

resource "aws_security_group" "sg_bastion" {
   name = "sg_bastion"
   description = "Security group for bastion host"
   vpc_id = module.vpc.vpc_id
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sg_db" {
  
}
output "sg_bastion_id" {
  value = aws_security_group.sg_bastion.id
}

output "sg_wordpress_id" {
  value = aws_security_group.sg_wordpress.id
}
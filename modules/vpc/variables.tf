variable "cidr_block" {
  type = string
  description = "Plage cidr pour le vpc"
  default = "10.0.0.0/16"
}

variable "enable_dns_hostnames"{
  type = bool
  description = "Support dns hostnames"
  default = true
}

variable "enable_dns_support" {
  type = bool
  description = "Support dns"
  default = true
}
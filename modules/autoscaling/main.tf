module "vpc" {
  source = "../vpc"
}

resource "aws_launch_configuration" "wordpress_launch_config" {
  name          = "wordpress-launch-config"
  image_id      = data.aws_ami.wordpress_ami.id
  instance_type = "t2.micro"
  # Autres paramètres de configuration comme les scripts de démarrage, les clés SSH, etc.
}

resource "aws_autoscaling_group" "wordpress_asg" {
  name                 = "wordpress-asg"
  min_size             = 1
  max_size             = 2
  desired_capacity     = 1
  vpc_zone_identifier  = [module.vpc.evaluation_wordpress_subnet_a_id, module.vpc.evaluation_wordpress_subnet_b_id]
  launch_configuration = aws_launch_configuration.wordpress_launch_config.name
  # Autres paramètres de configuration comme les groupes de sécurité, les stratégies de mise à l'échelle, etc.
}

resource "aws_lb_target_group" "wordpress_target_group" {
  name        = "wordpress-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id

}


